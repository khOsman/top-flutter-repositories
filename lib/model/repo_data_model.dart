class RepoDataModel{
   String? name;
   String? fullName;
   String? avatar_url;
   String? description;
   String? updated_at;
   int? watchers_count;
   int? forks_count;

   List<dynamic> topics=[];

  RepoDataModel({
    required this.name,
    required this.fullName,
    required this.avatar_url,
    required this.description,
    required this.watchers_count,
    required this.forks_count,
    required this.topics,
    required this.updated_at});

  RepoDataModel.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    fullName = json['full_name'];
    avatar_url = json['avatar_url'];
    description = json['description'];
    watchers_count = json['watchers_count'];
    forks_count = json['forks_count'];
    topics = json['topics'];
    updated_at = json['updated_at'];
  }

  Map<String, dynamic> toJson() {
    return {
      "name": name,
      "full_name": fullName,
      "avatar_url": avatar_url,
      "description": description,
      "watchers_count": watchers_count,
      "forks_count": forks_count,
      "topics": topics,
      "updated_at": updated_at,
    };
  }

   static List<RepoDataModel> sortByUpdatedAt(List<RepoDataModel> repoList, {bool ascending = true}) {
     repoList.sort((a, b) {
       final aDate = DateTime.parse(a.updated_at!);
       final bDate = DateTime.parse(b.updated_at!);
       return ascending ? aDate.compareTo(bDate) : bDate.compareTo(aDate);
     });
     return List<RepoDataModel>.from(repoList);
   }
}