import 'package:top_flutter_repositories/cache/cache_data.dart';
import 'package:top_flutter_repositories/model/repo_data_model.dart';
import 'package:dio/dio.dart';
import 'package:top_flutter_repositories/service/api_constant/api_endpoints.dart';
import 'package:top_flutter_repositories/utils/utils.dart';
class ApiService{

  Future<List<RepoDataModel>>getRepoList(int page) async{
    List<RepoDataModel> repos=[];
    final Dio dio = Dio();
    try{
      var response = await dio.get(ApiEndPoint.getSearchRepoApi(page, Constants.per_page));
      if(response.statusCode==200){
        var items=response.data["items"];
        // making dynamic string data list from response data
        List<Map<String, dynamic>> jsonData = List<Map<String, dynamic>>.from(items);
        repos = jsonData.map((repo) => RepoDataModel(
            name: repo["name"],
            fullName: repo["full_name"],
            avatar_url: repo["owner"]["avatar_url"],
            description: repo["description"],
            watchers_count: repo["watchers_count"],
            forks_count:repo["forks_count"] ,
            topics: repo["topics"],
            updated_at: repo["updated_at"]
        )).toList();

        CacheData cacheData = CacheData();
        final currentTime = DateTime.now().millisecondsSinceEpoch;
        cacheData.setTheLastFetchedDataTime(currentTime);
        print("Response ${repos[0].updated_at}");
      }
    }
    catch(err){
      throw err;
    }

    return repos;
  }


}

enum SearchPeremeters{
  flutter,
  page,
  per_page
}