class ApiEndPoint{
  static const String BASE_URL = "https://api.github.com";
  // https://api.github.com/search/repositories?q=flutter&page=1&per_page=2

  static String getSearchRepoApi(int page,int per_page_data){
    String search_repositories = "/search/repositories?q=flutter&page=${page}&per_page=${per_page_data}";
    return BASE_URL+search_repositories;
  }
}

