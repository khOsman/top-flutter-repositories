import 'package:flutter/material.dart';

class AppSizes {
  static const double splashScreenTitleFontSize = 48;
  static const double titleFontSize = 20;
  static const double subTitleFontSize = 16;
  static const double cardTitleFontSize = 16;
  static const double cardSubTitleFontSize = 12;
  static const double bodyFontSize = 14;
  static const double sidePadding = 15;
  static const double widgetSidePadding = 20;
  static const double buttonRadius = 25;
  static const double imageRadius = 8;
  static const double linePadding = 4;
  static const double widgetBorderRadius = 34;
  static const double textFieldRadius = 4.0;
  static const EdgeInsets bottomSheetPadding = EdgeInsets.symmetric(horizontal: 16, vertical: 10);
  static const app_bar_size = 56.0;
  static const app_bar_expanded_size = 180.0;
  static const tile_width = 148.0;
  static const tile_height = 276.0;

}

class AppColors {

  static const black = Color(0xFF24292d);
  static const lightBlack = Color(0xFF2b3137);
  static const darkGray = Color(0xFF979797);
  static const white = Color(0xFFFFFFFF);
  static const blue = Color(0xFF0d74e7);
  static const background = Color(0xFFE5E5E5);
  static const backgroundLight = Color(0xFFF9F9F9);
  static const transparent = Color(0x00000000);
  static const success = Color(0xFF2AA952);
  static const green = Color(0xFF2fbb4f);
  static const MaterialColor appMaterialColor = Colors.blueGrey;
}

class AppConsts {
  static const page_size = 10;
}

class AppStrings{
  static const String homeScreenTitle="Home";
  static const String detailScreenTitle="Details";
}


class AppMediaQuery {
  static double width(BuildContext context){
    return MediaQuery.of(context).size.width;
  }
  static double height(BuildContext context){
    return MediaQuery.of(context).size.height;
  }
}