import 'package:internet_connection_checker/internet_connection_checker.dart';
import 'package:intl/intl.dart';

class CheckConnection{

  static Future<bool> hasInternet()async{
    bool result = await InternetConnectionChecker().hasConnection;
    if(result == true) {
      print('YAY! you are connected');
    } else {
      print('No internet :( Reason:');
      print(InternetConnectionChecker().checkTimeout);
    }
    return result;
  }

}

class Formatter{

  static String convertDateFormat(String input) {
    final dateTime = DateTime.parse(input).toLocal();
    final formatter = DateFormat('MM-dd-yyyy HH:mm');
    return formatter.format(dateTime);
  }

  static String truncateStringWithEllipsis(String text, int maxLength) {
    if (text.length <= maxLength) {
      return text;
    } else {
      return '${text.substring(0, maxLength)}...';
    }
  }


}

class Constants{
  static int timer=30;
  static int per_page=10;
}

