import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';
import 'package:top_flutter_repositories/model/repo_data_model.dart';

class CacheData{

  Future<void> saveRepositoriesToCache(List<RepoDataModel> repoData) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final repoDataList = repoData.toList(); // Convert the set to a list for serialization
    final encodedData = repoDataList.map((repo) => repo.toJson()).toList();
    await prefs.setStringList('repo-list', encodedData.map((e) => json.encode(e)).toList());
  }

  Future<List<RepoDataModel>> loadRepoListFromCache() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final encodedData = prefs.getStringList('repo-list');
    if (encodedData != null) {
      final repoDataList = encodedData.map((jsonString) => RepoDataModel.fromJson(json.decode(jsonString))).toList();
      return List<RepoDataModel>.from(repoDataList);
    } else {
      return [];
    }
  }

  Future<void> cleanRepositoriesData() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("repo-list");
  }


  Future<void> setTheLastFetchedDataTime(int currentTime) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // Update the last refresh time to the current time
    prefs.setInt('lastRefreshTime', currentTime);
  }

  Future<int> getTheLastFetchedDataTime() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // Get the last refresh time from SharedPreferences (defaulting to 0 if it's not available)
    final lastRefreshTime = prefs.getInt('lastRefreshTime') ?? 0;
    return lastRefreshTime;
  }


  Future<void> fetchDataIfNeeded(int timeLimit,Function() onCallBack) async {
    // Get the last refresh time from SharedPreferences (defaulting to 0 if it's not available)
    final lastRefreshTime = await getTheLastFetchedDataTime();

    final currentTime = DateTime.now().millisecondsSinceEpoch;
    final differenceInMinutes = ((currentTime - lastRefreshTime) / 60000).round();

    if (differenceInMinutes >= timeLimit) {
      // If 30 minutes have passed since the last refresh, fetch new data from the API
      // ... your code to fetch data from the API
      print("Timer !!!!");
      onCallBack();
    }
  }

}