import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:top_flutter_repositories/cache/cache_data.dart';
import 'package:top_flutter_repositories/config/app_theme.dart';
import 'package:top_flutter_repositories/presentation/features/home/bloc/repoList_bloc.dart';
import 'package:top_flutter_repositories/presentation/features/home/bloc/repoList_event.dart';
import 'package:top_flutter_repositories/presentation/features/home/bloc/repoList_state.dart';
import 'package:top_flutter_repositories/presentation/widgets/common_widgets.dart';
import 'package:top_flutter_repositories/presentation/widgets/views.dart';
import 'package:connectivity/connectivity.dart';
import 'package:top_flutter_repositories/utils/utils.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {

  late Connectivity _connectivity;
  late StreamSubscription<ConnectivityResult> _connectivitySubscription;

  void internetConnectionReestablished({required Function onConnection}){
    _connectivity = Connectivity();
    _connectivitySubscription = _connectivity.onConnectivityChanged.listen((result) {
      if (result == ConnectivityResult.mobile || result == ConnectivityResult.wifi) {
        // Connection is re-established (either mobile data or Wi-Fi)
        print('Internet connection re-established.');
        onConnection();
      }
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    
    internetConnectionReestablished(onConnection: (){
      // It will try to fetch data from Internet first
      // If it fails then it will try to get data from cache
      context.read<RepoListBloc>().add(GetRepoList());
    });
    getDataFromCacheOnNetFailure();

    //Scheduling
    Timer.periodic(Duration(minutes: Constants.timer), (timer) async {
      CacheData cacheData = CacheData();
      cacheData.fetchDataIfNeeded(Constants.timer,() => context.read<RepoListBloc>().add(GetRepoList()));
    });

  }

  Future<void> getDataFromCacheOnNetFailure() async {
    bool hasInternet = await CheckConnection.hasInternet();
    if(!hasInternet) context.read<RepoListBloc>().add(GetRepoList());
  }

  @override
  void dispose() {
    _connectivitySubscription.cancel();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {
    bool isFetching = false;
    return Scaffold(
      appBar: CommonWidgets.setAppBar(context,title: AppStrings.homeScreenTitle,hasSortOption: true,onSortAction: (asc){
        context.read<RepoListBloc>().add(SortRepoList(isAcendening: asc));
      }),
      body: BlocBuilder<RepoListBloc,RepoListState>(
        builder:(context,state){
         if(state is RepoListLoading) {
            print("Loading state");
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
         if(state is RepoListError) {
           return Center(
             child:CommonWidgets.setText(title: state.errorMessage, fontSize: AppSizes.bodyFontSize),
           );
         }

         if(state is RepoListNotLoaded) {
           return Center(
             child:CommonWidgets.setText(title: state.message, fontSize: AppSizes.bodyFontSize),
           );
         }

         if(state is RepoListLoaded) {
           print("Loaded state");
           isFetching = false;
           return NotificationListener<ScrollNotification>(
               onNotification: (ScrollNotification scrollInfo) {
                 if (!isFetching &&
                     scrollInfo.metrics.pixels >=
                         scrollInfo.metrics.maxScrollExtent) {
                   isFetching = true;
                   context.read<RepoListBloc>().add(FetchNextPage());
                 }
                 return false;
               },
               child: Views.listView(context, state.repoList!));
         }

         return Container();
        },
      ),
    );
  }


}
