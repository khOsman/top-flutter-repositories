import 'package:equatable/equatable.dart';

abstract class RepoListEvent extends Equatable{
}

class GetRepoList extends RepoListEvent{
  @override
  // TODO: implement props
  List<Object?> get props => [];
}

class SortRepoList extends RepoListEvent{
  bool isAcendening;
  SortRepoList({required this.isAcendening});
  @override
  // TODO: implement props
  List<Object?> get props => [isAcendening];
}

class FetchNextPage extends RepoListEvent {

  @override
  List<Object?> get props => [];
}


