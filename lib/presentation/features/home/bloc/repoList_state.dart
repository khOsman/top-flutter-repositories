import 'package:equatable/equatable.dart';
import 'package:top_flutter_repositories/model/repo_data_model.dart';


abstract class RepoListState extends Equatable{

}

class RepoListInit extends RepoListState{
  @override
  // TODO: implement props
  List<Object?> get props => [];

}

class RepoListLoading extends RepoListState{
  @override
  // TODO: implement props
  List<Object?> get props => [];

}

class RepoListLoaded extends RepoListState{

  List<RepoDataModel> repoList;

  RepoListLoaded( this.repoList);

@override
  // TODO: implement props
  List<Object?> get props => [repoList];
}

class RepoListNotLoaded extends RepoListState{
  String message;

  RepoListNotLoaded( this.message);

  @override
  // TODO: implement props
  List<Object?> get props => [message];

}


class RepoListError extends RepoListState{

  String errorMessage;
  RepoListError(this.errorMessage);

  @override
  // TODO: implement props
  List<Object?> get props => [errorMessage];
}

