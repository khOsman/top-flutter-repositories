import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:top_flutter_repositories/cache/cache_data.dart';
import 'package:top_flutter_repositories/model/repo_data_model.dart';
import 'package:top_flutter_repositories/presentation/features/home/bloc/repoList_event.dart';
import 'package:top_flutter_repositories/presentation/features/home/bloc/repoList_state.dart';
import 'package:top_flutter_repositories/service/api_service.dart';
import 'package:top_flutter_repositories/utils/utils.dart';
class RepoListBloc extends Bloc<RepoListEvent,RepoListState>{

  List<RepoDataModel> repoDataList=[];
  int currentPage = 1;


  RepoListBloc():super(RepoListInit()){

    // Service instance
    ApiService apiService = ApiService();
    CacheData cacheData = CacheData();


    on<FetchNextPage>((event,emit)async{
      currentPage++;
      emit(RepoListLoading());
      fetchNextData(apiService, cacheData);
    });

    on<SortRepoList>((event,emit)async{
        emit(RepoListLoading());
        repoDataList = RepoDataModel.sortByUpdatedAt(repoDataList,ascending: event.isAcendening);
        emit(RepoListLoaded( repoDataList));
    });


    on<GetRepoList>((event, emit) async {
      try{
        //emit loading state
        emit(RepoListLoading());
        fetchData(apiService, cacheData);
      }
      catch(err)
      {
        // emit the err state
        emit(RepoListError( "Error: "+err.toString()));
      }
    });

  }

  Future<void> fetchNextData(ApiService apiService,CacheData cacheData) async {
    //Check internet status
    bool hasInternet = await CheckConnection.hasInternet();
    // try to fetch the data & emit the data to be loaded
    if(hasInternet) {
      List<RepoDataModel> newRepos = await apiService.getRepoList(currentPage);
      // add new data to the list
      repoDataList.addAll(newRepos);
      // cache the fetched data & get the data
      await cacheData.saveRepositoriesToCache(repoDataList);
      repoDataList = await cacheData.loadRepoListFromCache();
      emit(RepoListLoaded( repoDataList));
    }else {
      //get the data from cache
      repoDataList = await cacheData.loadRepoListFromCache();
      // Check cache has data?
      if(repoDataList.length==0) emit(RepoListNotLoaded( "No Internet Connection\nNo cache data found :("));
      else emit(RepoListLoaded( repoDataList));
    }
  }

  Future<void> fetchData(ApiService apiService,CacheData cacheData) async {
    //Check internet status
    bool hasInternet = await CheckConnection.hasInternet();
    // try to fetch the data & emit the data to be loaded
    if(hasInternet) {
      var cacheDataList = await cacheData.loadRepoListFromCache();
      if(cacheDataList.isEmpty){
        List<RepoDataModel> newRepos = await apiService.getRepoList(currentPage);
        // add new data to the list
        repoDataList.addAll(newRepos);
        // cache the fetched data & get the data
        await cacheData.saveRepositoriesToCache(repoDataList);
      }
      repoDataList = await cacheData.loadRepoListFromCache();
      emit(RepoListLoaded( repoDataList));
    }else {
      //get the data from cache
      repoDataList = await cacheData.loadRepoListFromCache();
      // Check cache has data?
      if(repoDataList.length==0) emit(RepoListNotLoaded( "No Internet Connection\nNo cache data found :("));
      else emit(RepoListLoaded( repoDataList));
    }
  }



}