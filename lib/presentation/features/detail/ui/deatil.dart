import 'package:flutter/material.dart';
import 'package:top_flutter_repositories/config/app_theme.dart';
import 'package:top_flutter_repositories/model/repo_data_model.dart';
import 'package:top_flutter_repositories/presentation/widgets/common_widgets.dart';
import 'package:top_flutter_repositories/presentation/widgets/views.dart';
import 'package:top_flutter_repositories/utils/utils.dart';

class DetailPage extends StatefulWidget {
  RepoDataModel dataModel;
  DetailPage({Key? key,required this.dataModel}) : super(key: key);

  @override
  State<DetailPage> createState() => _DetailPageState();
}

class _DetailPageState extends State<DetailPage> {

  bool hasInternet =false;
  RepoDataModel? _dataModel;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    checkInternet();
    _dataModel= widget.dataModel;

  }

  Future<void> checkInternet() async {
    bool _hasInternet = await CheckConnection.hasInternet();
    setState(() {
      hasInternet=_hasInternet;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CommonWidgets.setAppBar(context,
          title: "${Formatter.truncateStringWithEllipsis(_dataModel!.fullName!.split('/')[1], 20)} ${AppStrings.detailScreenTitle}"),
      body: Views.detailView(context, _dataModel!,hasInternet),
    );
  }
}
