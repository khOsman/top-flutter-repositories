import 'package:flutter/material.dart';
import 'package:top_flutter_repositories/config/app_theme.dart';


class CommonWidgets{


  static Widget setText({required String title, required double fontSize ,bool isBold=false, bool isCenter=false}){
    return Text(title,
      style:TextStyle(
        fontSize: fontSize,
        fontWeight: isBold?FontWeight.bold:FontWeight.normal
      ),
      textAlign: isCenter?TextAlign.center:TextAlign.start,
    );
  }

  static AppBar setAppBar(BuildContext context,{required String title, bool hasSortOption=false, Function(bool)? onSortAction}){

    return AppBar(
      backgroundColor: AppColors.black,
      title: CommonWidgets.setText(title: title,fontSize: AppSizes.titleFontSize),
      actions: !hasSortOption?[]:[
        PopupMenuButton<String>(
            icon: Icon(Icons.sort),
            onSelected: (String value)async{
              if(value=="asc"){
                onSortAction!(false);
              }
              if(value=="des"){
                onSortAction!(true);
              }
            },
            itemBuilder: (context){
              return <PopupMenuEntry<String>>[
                PopupMenuItem<String>(
                    value:'asc',
                    child:Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: setText(title: "Ascending", fontSize: AppSizes.bodyFontSize,isBold: true),
                    ) ),
                PopupMenuItem<String>(
                    value:'des',
                    child:Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: setText(title: "Descending", fontSize: AppSizes.bodyFontSize,isBold: true),
                    ) ),
              ];
            })
      ],
    );
  }

  static void changeScreen({required BuildContext context, required Widget screen}){
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => screen,
      ),
    );
  }

}