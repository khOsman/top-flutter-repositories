import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:top_flutter_repositories/config/app_theme.dart';
import 'package:top_flutter_repositories/model/repo_data_model.dart';
import 'package:top_flutter_repositories/presentation/features/detail/ui/deatil.dart';
import 'package:top_flutter_repositories/presentation/widgets/common_widgets.dart';
import 'package:top_flutter_repositories/utils/utils.dart';

class Views{
  static Widget listView(BuildContext context, List<RepoDataModel> data){
    return ListView.builder(
      itemBuilder: (context,index){
        if(index<data.length){
          RepoDataModel repoDataModel = data[index];
          return tileView(context, index+1,repoDataModel);
        }else{
          return const Center(
            child: CircularProgressIndicator(),
          );
        }
      },
      itemCount: data.length+1,
    );
  }

  static Widget tileView(BuildContext context, int id,RepoDataModel repoDataModel ){
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: GestureDetector(
        onTap: (){
          print("tapped ${repoDataModel.name!}");
          CommonWidgets.changeScreen(context: context, screen: DetailPage(dataModel: repoDataModel!));
        },
        child: Container(
          height: 80 ,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: AppColors.background
          ),
          child:Padding(
            padding: const EdgeInsets.all(8.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                CommonWidgets.setText(title: id.toString()+".", fontSize: AppSizes.cardTitleFontSize),
                SizedBox(width: 10,),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CommonWidgets.setText(title: repoDataModel.name!.toUpperCase(), fontSize: AppSizes.cardTitleFontSize),
                    SizedBox(height: 15,),
                    CommonWidgets.setText(title: repoDataModel.fullName!.toUpperCase(), fontSize: AppSizes.cardSubTitleFontSize),
                  ],
                )
              ],
            ),
          )
        ),
      ),
    );
  }

  static Widget detailView(BuildContext context,RepoDataModel _dataModel, bool hasInternet){
    return Container(
      height: AppMediaQuery.height(context),
      width:  AppMediaQuery.width(context),

      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 10,),
          //Repo Owner Info
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              // Owner Profile & name
              Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,

                children: [
                    CachedNetworkImage(
                    imageUrl: _dataModel!.avatar_url!,
                    placeholder: (context, url) => CircularProgressIndicator(), // Placeholder widget until the image is loaded
                    errorWidget: (context, url, error) => Icon(Icons.error), // Widget shown when image fails to load
                    width: 130, // Set the width as per your design
                    height: 130, // Set the height as per your design
                    fit: BoxFit.cover, // Adjust the fit as needed
                  ),
                ],
              ),
              //Repository Name -
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  CommonWidgets.setText(title: "Repository Owner", fontSize: AppSizes.titleFontSize,isBold: true),
                  SizedBox(height: 5,),
                  CommonWidgets.setText(title: _dataModel!.fullName!.split('/')[0], fontSize: AppSizes.subTitleFontSize),
                  SizedBox(height: 5,),

                ],
              )
            ],
          ),
          SizedBox(height: 20,),
          // Repo Name, fork & watch
          Column(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  CommonWidgets.setText(title: "Repository Name", fontSize: AppSizes.titleFontSize,isBold: true),
                  SizedBox(height: 5,),
                  CommonWidgets.setText(title: _dataModel!.fullName!, fontSize: AppSizes.bodyFontSize),
                ],
              ),
              SizedBox(height: 10,),
              // Fork & Watch
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Row(
                    children: [
                      Image.asset("assets/fork.png",
                        height: 24,
                        width: 24,
                        fit: BoxFit.fitHeight,
                      ),
                      SizedBox(width: 10,),
                      CommonWidgets.setText(title: _dataModel!.forks_count!.toString(), fontSize: AppSizes.bodyFontSize),
                    ],
                  ),
                  SizedBox(width: 20,),
                  Row(
                    children: [
                      Image.asset("assets/watch.png",
                        height: 24,
                        width: 24,
                        fit: BoxFit.fitHeight,
                      ),
                      SizedBox(width: 10,),
                      CommonWidgets.setText(title: _dataModel!.watchers_count!.toString(), fontSize: AppSizes.bodyFontSize),
                    ],
                  ),
                ],
              ),
              SizedBox(height: 10,),
              CommonWidgets.setText(title: "Last Update", fontSize: AppSizes.subTitleFontSize, isBold:true),
              SizedBox(height: 5,),
              CommonWidgets.setText(title: Formatter.convertDateFormat(_dataModel!.updated_at!), fontSize: AppSizes.subTitleFontSize),
            ],
          ),
          SizedBox(height: 15,),
          // Repo description
          Column(
            children: [
              CommonWidgets.setText(title: "Description", fontSize: AppSizes.titleFontSize,isBold: true),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: CommonWidgets.setText(title: _dataModel!.description!, fontSize: AppSizes.subTitleFontSize,isCenter: true),
              ),
            ],
          ),
          SizedBox(height: 15,),
          CommonWidgets.setText(title: "Topics", fontSize: AppSizes.titleFontSize,isBold: true),
          SizedBox(height: 15,),
          //Topic lists
          Expanded(child: ListView.builder(
            itemCount: _dataModel!.topics!.toList().length,
            itemBuilder: (context,index){
              var topics= _dataModel!.topics!.toList();
              if(topics.isNotEmpty){
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.background,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: CommonWidgets.setText(title: topics[index].toString().toUpperCase(), fontSize: AppSizes.subTitleFontSize,isCenter: true),
                  ),
                );
              }
              else {
                return CommonWidgets.setText(title: "No Topics Found", fontSize: AppSizes.subTitleFontSize);
              }
            },
          ),
          ),
        ],
      ),
    );
  }


}