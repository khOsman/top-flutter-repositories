# Top-Repositories-Details

## App Screenshots
 # Here are a few examples of what can be shown:

  <p align="center">
      <img src="https://drive.google.com/uc?export=view&id=1eMmw-RTGhzFsOud8A7Z3ad7IDnIfB81w" width="250" alt="Home screen displaying the repository list" />
      <img src="https://drive.google.com/uc?export=view&id=1dmPV7wx1pyz3Rpom5x6OTvlTnbP5Zlct" width="250" alt="Details screen of a repository"/>
      <img src="https://drive.google.com/uc?export=view&id=1eFVXW0xuxzaq06ZVvE5_m1D31CESYfXb" width="250" alt="Sorting options"/>
  </p>
  <p align="center">
      <img src="https://drive.google.com/uc?export=view&id=1eItawzWoeEQlL3_lrLOEkyzo-sEgC-ej" width="250" alt="Pagination feature]" />
      <img src="https://drive.google.com/uc?export=view&id=1dasmyRB5N21QpR64kNoxlcrCR6Fzl2jG" width="250" alt="Offline mode detail display"/>
      <img src="https://drive.google.com/uc?export=view&id=1eWY6ivdMLGKa0wMhseUmWJDfOVqFHtyP" width="250" alt="No Internet & Cache"/>
  </p>

## Clean Code & Architecture
- **BLoC Architecture**: The project uses the BLoC (Business Logic Component) architecture for managing state and business logic, providing separation of concerns and easy testing.
- **Clean Code Practices**: Emphasize clean, readable, and maintainable code. Following naming conventions, proper documentation, and appropriate code organization.


# Libraries Used

# --- Bloc ---
  - flutter_bloc: ^8.1.3
  - equatable: ^2.0.5

# --- Network and HTTP request handler ---
  - internet_connection_checker: ^1.0.0+1
  - connectivity: ^3.0.6
  - dio: ^5.3.3

# --- Cache ---
  - shared_preferences: ^2.2.2
  - cached_network_image: ^3.1.0

# --- Formatter ---
  - intl: ^0.17.0


## Task Completion

✅ Fetch the repository list from GitHub API using "Flutter" as the query keyword. 

✅ The fetched data is stored in a local database to support offline mode.

✅ Pagination: The repository list is fetched in pages of 10 items by scrolling.

✅ Data Refresh: Repositories can be refreshed from the API, but not more frequently than once every 30 minutes.

✅ Home Page: Display a list of repositories.

✅ Sorting: Implement a sorting feature allowing sorting by last updated date-time or star count.

✅ Sorting Persistence: The selected sorting option persists across app sessions.

✅ Repo Details Page: Clicking on a list item navigates to a details page, displaying repo owner's name, photo, repository description, last update date-time (formatted as month-day-year hour:seconds), and additional desired fields.

✅ Offline Browsing: Data loaded once, both repository list and details, are saved for offline access.

